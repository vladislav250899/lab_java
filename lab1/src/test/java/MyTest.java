import static org.testng.Assert.assertEquals;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.Arrays;

public class MyTest {

    @Test(dataProvider = "salaryProvider")
    public void salaryTest(EmployeePayrolls E, LocalDate d, double res)
    {
        assertEquals(E.salary(d), res);
    }

    @DataProvider
    public Object[][] salaryProvider()
    {
        return new Object[][]{new Object[]{new EmployeePayrolls(new Employee("John", "Snow", new Position("The Bastard of Winterfell", (double) 200000), "jsnow@gmail.com", LocalDate.of(1993, 12, 11), LocalDate.of(2011, 9, 21), "man"), Arrays.asList(new Payroll(1100, LocalDate.of(2017, 1, 1)), new Payroll(1200, LocalDate.of(2017, 2, 1))
                , new Payroll(1300, LocalDate.of(2017, 3, 1)), new Payroll(1400, LocalDate.of(2017, 4, 1))
                , new Payroll(1500, LocalDate.of(2017, 5, 1)), new Payroll(1600, LocalDate.of(2017, 6, 1))
                , new Payroll(1700, LocalDate.of(2017, 7, 1)), new Payroll(1800, LocalDate.of(2017, 8, 1))
                , new Payroll(1900, LocalDate.of(2017, 9, 1)), new Payroll(2000, LocalDate.of(2017, 10, 1))
                , new Payroll(2100, LocalDate.of(2017, 11, 1)), new Payroll(2200, LocalDate.of(2017, 12, 1)))), LocalDate.of(2017, 7, 1), (double) 201700},
                {new EmployeePayrolls(new Employee("Daenerys", "Targaryen",new Position("The Mother of Dragons",(double) 2000000),"mommyOfDrago@gmail.com",LocalDate.of(1995,7,22),LocalDate.of(2012,1,12),"woman"),Arrays.asList(new Payroll(1100, LocalDate.of(2017, 1, 1)), new Payroll(1200, LocalDate.of(2017, 2, 1))
                        , new Payroll(1300, LocalDate.of(2017, 3, 1)), new Payroll(1400, LocalDate.of(2017, 4, 1))
                        , new Payroll(1500, LocalDate.of(2017, 5, 1)), new Payroll(1600, LocalDate.of(2017, 6, 1))
                        , new Payroll(1700, LocalDate.of(2017, 7, 1)), new Payroll(1800, LocalDate.of(2017, 8, 1))
                        , new Payroll(1900, LocalDate.of(2017, 9, 1)), new Payroll(2000, LocalDate.of(2017, 10, 1))
                        , new Payroll(2100, LocalDate.of(2017, 11, 1)), new Payroll(2200, LocalDate.of(2017, 12, 1)))),LocalDate.of(2017,12,1), (double) 2002200}};
    }

    @Test(dataProvider = "ageProvider")
    public void ageTest(Employee E, boolean res)
    {
        assertEquals(E.isRetirementAge(), res);
    }

    @DataProvider
    public Object[][] ageProvider()
    {
        return new Object[][]{{new Employee("John", "Snow",new Position("The Bastard of Winterfell",(double) 200000),"jsnow@gmail.com",LocalDate.of(1993,12,11),LocalDate.of(2011,9,21),"man"), false},
                {new Employee("Daenerys", "Targaryen",new Position("The Mother of Dragons",(double) 2000000),"mommyOfDrago@gmail.com",LocalDate.of(1957,7,22),LocalDate.of(2012,1,12),"woman"),  true}};
    }
}
