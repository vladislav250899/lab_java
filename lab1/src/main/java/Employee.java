import javax.swing.*;
import java.util.*;
import java.time.LocalDate;


//створити клас Працівник-зарплати, де поле Працівник, і список Зарплат, і обч ф-я зарплата
//створити константу пенсійний вік, для жінок і чоловіків, додати поле Стать
public class Employee {
    private String name;
    private String surname;
    private Position pos;
    private String email;
    private LocalDate dateOfBirth;
    private LocalDate dateOfEmployment;
    private String gender;
    private final int MEN_RETIREMENT_AGE=60;
    private final int WOMEN_RETIREMENT_AGE=58;


    public String getGender() {
        return gender;
    }

    public void setGender(String gender) { assert (gender.equals("man")||gender.equals("woman"));
        this.gender = gender;
    }

    public int getMEN_RETIREMENT_AGE() {
        return MEN_RETIREMENT_AGE;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employee)) return false;
        Employee employee = (Employee) o;
        return getMEN_RETIREMENT_AGE() == employee.getMEN_RETIREMENT_AGE() &&
                getWOMEN_RETIREMENT_AGE() == employee.getWOMEN_RETIREMENT_AGE() &&
                Objects.equals(getName(), employee.getName()) &&
                Objects.equals(getSurname(), employee.getSurname()) &&
                Objects.equals(getPos(), employee.getPos()) &&
                Objects.equals(getEmail(), employee.getEmail()) &&
                Objects.equals(getDateOfBirth(), employee.getDateOfBirth()) &&
                Objects.equals(getDateOfEmployment(), employee.getDateOfEmployment()) &&
                Objects.equals(getGender(), employee.getGender());
    }



    @Override
    public int hashCode() {
        return Objects.hash(getName(), getSurname(), getPos(), getEmail(), getDateOfBirth(), getDateOfEmployment(), getGender(), getMEN_RETIREMENT_AGE(), getWOMEN_RETIREMENT_AGE());
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", pos=" + pos +
                ", email='" + email + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", dateOfEmployment=" + dateOfEmployment +
                ", gender='" + gender + '\'' +
                ", MEN_RETIREMENT_AGE=" + MEN_RETIREMENT_AGE +
                ", WOMEN_RETIREMENT_AGE=" + WOMEN_RETIREMENT_AGE +
                '}';
    }

    public int getWOMEN_RETIREMENT_AGE() {
        return WOMEN_RETIREMENT_AGE;
    }

    public Employee(String name, String surname, Position pos, String email, LocalDate dateOfBirth, LocalDate dateOfEmployment, String gender) {
        assert (gender.equals("man")||gender.equals("woman"));
        this.name = name;
        this.surname = surname;
        this.pos = pos;
        this.email = email;
        this.dateOfBirth = dateOfBirth;
        this.dateOfEmployment = dateOfEmployment;
        this.gender=gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Position getPos() {
        return pos;
    }

    public void setPos(Position pos) {
        this.pos = pos;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public LocalDate getDateOfEmployment() {
        return dateOfEmployment;
    }

    public void setDateOfEmployment(LocalDate dateOfEmployment) {
        this.dateOfEmployment = dateOfEmployment;
    }


    /**
     * @return true if employees age is more than 60
     */
    public boolean isRetirementAge() {
        assert (gender.equals("man")||gender.equals("woman"));
        LocalDate date = LocalDate.now();
        if (gender.equals("man"))
            return (date.getYear() - dateOfBirth.getYear() > getMEN_RETIREMENT_AGE());
        if (gender.equals("woman"))
            return (date.getYear() - dateOfBirth.getYear() > getWOMEN_RETIREMENT_AGE());
    else {
        JOptionPane.showMessageDialog(null, "Не визначено чи працівник похилого віку, чи ні");
        return false;
    }
    }


}

