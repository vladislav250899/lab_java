import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class EmployeePayrolls {
    private Employee emp;
    private List<Payroll> payrolls; /*= Arrays.asList(new Payroll(1100, LocalDate.of(2017, 1, 1)), new Payroll(1200, LocalDate.of(2017, 2, 1))
            , new Payroll(1300, LocalDate.of(2017, 3, 1)), new Payroll(1400, LocalDate.of(2017, 4, 1))
            , new Payroll(1500, LocalDate.of(2017, 5, 1)), new Payroll(1600, LocalDate.of(2017, 6, 1))
            , new Payroll(1700, LocalDate.of(2017, 7, 1)), new Payroll(1800, LocalDate.of(2017, 8, 1))
            , new Payroll(1900, LocalDate.of(2017, 9, 1)), new Payroll(2000, LocalDate.of(2017, 10, 1))
            , new Payroll(2100, LocalDate.of(2017, 11, 1)), new Payroll(2200, LocalDate.of(2017, 12, 1)));*/

    @Override
    public String toString() {
        return "EmployeePayrolls{" +
                "emp=" + emp +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EmployeePayrolls)) return false;
        EmployeePayrolls that = (EmployeePayrolls) o;
        return Objects.equals(getEmp(), that.getEmp());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEmp());
    }

    public Employee getEmp() {
        return emp;
    }

    public void setEmp(Employee emp) {
        this.emp = emp;

    }

    public EmployeePayrolls(Employee emp, Payroll pay) {
        this.emp = emp;
        payrolls.add(pay);
    }

    public EmployeePayrolls(Employee emp,List<Payroll> payrolls) {
            this.payrolls = payrolls;
            this.emp=emp;

    }


    public void setPayrolls(List<Payroll> payrolls) {
        this.payrolls = payrolls;
    }

    public List<Payroll> getPayrolls() {
        return payrolls;
    }

    public void addPayrolls(Payroll pay) {
        payrolls.add(pay);
    }

    /**
     * @param dateOfPay is a date of payroll with bonus
     * @return salary with bonus
     */
    public double salary(LocalDate dateOfPay) {

        for (Payroll payroll : payrolls) {

            if (payroll.getDateOfPayroll().equals(dateOfPay)) {
                return emp.getPos().getSalary() + payroll.getBonus();
            }

        }
        return 0;
    }

}
