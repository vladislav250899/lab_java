

import java.util.Objects;

public class Position
{
    private String position;
    private Double salary;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Position)) return false;
        Position position1 = (Position) o;
        return Objects.equals(getPosition(), position1.getPosition()) &&
                Objects.equals(getSalary(), position1.getSalary());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPosition(), getSalary());
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public Position(String position, Double salary) {
        this.position = position;
        this.salary = salary;
    }
}
