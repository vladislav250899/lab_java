import java.time.LocalDate;
import java.util.Objects;

public class Payroll {
    private double bonus;
    private LocalDate dateOfPayroll;

    public Payroll(double bonus, LocalDate dateOfPayroll) {
        this.bonus = bonus;
        this.dateOfPayroll = dateOfPayroll;
    }
    public Payroll(Payroll pay) {
        this.bonus = pay.bonus;
        this.dateOfPayroll = pay.dateOfPayroll;
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    public LocalDate getDateOfPayroll() {
        return dateOfPayroll;
    }

    public void setDateOfPayroll(LocalDate dateOfPayroll) {
        this.dateOfPayroll = dateOfPayroll;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Payroll)) return false;
        Payroll payroll = (Payroll) o;
        return Double.compare(payroll.getBonus(), getBonus()) == 0 &&
                Objects.equals(getDateOfPayroll(), payroll.getDateOfPayroll());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBonus(), getDateOfPayroll());
    }

    @Override
    public String toString() {
        return "Payroll{" +
                "bonus=" + bonus +
                ", dateOfPayroll=" + dateOfPayroll +
                '}';
    }
}
