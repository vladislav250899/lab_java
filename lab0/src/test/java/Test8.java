import static org.testng.Assert.assertEquals;

import org.junit.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;



public class Test8
{
    public static double EPS = 0.0000001;
    @Test(dataProvider = "averageProvider")
    public void averageTest(float a,float b, float rez)
    {
        assertEquals(new Variant8().average(a,b), rez);
    }

    @DataProvider
    public Object[][] averageProvider()
    {
        return new Object[][]{{5,2, 3.5f}, {4,6, 5}};
    }

   /* @Test(expectedExceptions = AssertionError.class)
    public void negativeInputTest() {
        new Main().beginTask(-2);
    }*/

    //////////////////////////////////////////////////////////////////

    @Test(dataProvider = "productProvider")
    public void productTest(int a, int b, int res)
    {
        assertEquals(new Variant8().product(a,b),  res);
    }

    @DataProvider
    public Object[][] productProvider()
    {
        return new Object[][] { {1, 3, 6}, {3, 7, 2520 }, {10,12, 1320}};
    }

    @Test(expectedExceptions = AssertionError.class)
    public void negativeProductTest()
    {
        new Variant8().product(10, 2);
    }

    ///////////////////////////////////////////////////////////////////////////////////////

    @Test(dataProvider = "oddnessProvider")
    public void oddnessTest(int a, int b, boolean res)
    {
        assertEquals(new Variant8().oddness(a, b), res);
    }

    @DataProvider
    public Object[][] oddnessProvider()
    {
        return new Object[][] { {3, 5, true}, {3, 3, true}, {2, 4, false},{7, 4, false} };
    }

    //////////////////////////////////////////////////////////////////////////////////////

    @Test(dataProvider = "decreaseProvider")
    public void decreaseTest(int a, int b, Variant8.Pair res)
    {
        assertEquals(new Variant8().decrease(a,b), res);
    }

    @DataProvider
    public Object[][] decreaseProvider()
    {
        return new Object[][] { {1, 2, new Variant8.Pair(2,1)}, {4, 3, new Variant8.Pair(4,3)}, {5, 5, new Variant8.Pair(5,5)} };
    }

    //////////////////////////////////////////////////////////////////////////////////////

    @Test(dataProvider = "NextDateProvider")
    public void switchTest(int D,int M, Variant8.Pair res)
    {
        assertEquals(new Variant8().nextDate(D,M), res);
    }

    @DataProvider
    public Object[][] NextDateProvider()
    {
        return new Object[][] { {2, 3,new Variant8.Pair(3,3)}, {31, 12,new Variant8.Pair(1,1)}, {25, 8,new Variant8.Pair(26,8)} };
    }

    @Test(expectedExceptions = AssertionError.class)
    public void rangeSwitchTest()
    {
        new Variant8().nextDate(32,13);
    }

    //////////////////////////////////////////////////////////////////////////////////////

    @Test(dataProvider = "inversionProvider")
    public void productTest(int x, int res)
    {
        assertEquals(new Variant8().inversion(x), res);
    }

    @DataProvider
    public Object[][] inversionProvider()
    {
        return new Object[][] { {23,32}, {10,1}};
    }

    @Test(expectedExceptions = AssertionError.class)
    public void negativeInputForTest()
    {
        new Variant8().inversion(9);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////

    @Test (dataProvider = "searchKProvider")
    public void whileTest(int n, int res)
    {
        assertEquals(new Variant8().searchK(n), res);
    }

    @DataProvider
    public Object[][] searchKProvider()
    {
        return new Object[][] { {10, 3}, {16, 4}, {1, 1} };
    }

    @Test(expectedExceptions = AssertionError.class)
    public void negativeInputWhileTest()
    {
        new Variant8().searchK(-1);
    }

    /////////////////////////////////////////////////////////////////////////

    @Test (dataProvider = "checkProgressionProvider")
    public void checkProgressionTest(int[] array, float res)
    {
        assertEquals(new Variant8().checkProgression(array), res);
    }

    @DataProvider
    public Object[][] checkProgressionProvider()
    {
        return new Object[][] { { new int[]{5, 10, 20, 40, 80, 160},  2},
                { new int[]{3, 12, 48, 192, 768}, 4 }, {new int[]{3, 12, 48, 192, 762}, 0 }};
    }

    @Test(expectedExceptions = AssertionError.class)
    public void badRangeArrayTest()
    {
        new Variant8().checkProgression(new int[]{2, 0, 0, 0, 6, 24, 2} );
    }

    //////////////////////////////////////////////////////////////////////////////

    @Test(dataProvider = "matrixProvider")
    public void twoDimensionArrayTest(int[][] input, int[][] output)
    {
        Assert.assertArrayEquals(new Variant8().swap(input), output);
    }

    @DataProvider
    public Object[][] matrixProvider() {

        int[][] input1 = new int[][] {
                new int[] {-2, 3, -6, 9},
                new int[] {-34, -9, 98, 2},
                new int[] {-6, 2, 1, -4},
                new int[] {-8, -98, 2, 5}
        };

        int[][] output1 = new int[][] {
                new int[] {9, 3, -6, -2},
                new int[] {2, -9, 98, -34},
                new int[] {-4, 2, 1, -6},
                new int[] {5, -98, 2, -8}
        };

        int[][] input2 = new int[][] {
                new int[] {-5, 18},
                new int[] {-13, 0},
                new int[] {-12, 36},
                new int[] {-1, -7}
        };

        int[][] output2 = new int[][] {
                new int[] {18, -5},
                new int[] {0, -13},
                new int[] {36, -12},
                new int[] {-7, -1}
        };

        return new Object[][] { {input1, output1}, {input2, output2} };
    }
}
