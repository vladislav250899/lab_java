


import java.util.Objects;




public class Variant8 {

    public static final double EPSILON = 0.0001;

    /**
     *
     * @param a is integer number
     * @param b is integer number
     * @return average number
     */
    public  float average (float a,float b)
    {

        return (a+b)/2;
    }
    public static class Pair {
        private int first;
        private int second;

        @Override
        public String toString() {
            return "Pair{" +
                    "first=" + first +
                    ", second=" + second +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Pair pair = (Pair) o;
            return Math.abs(pair.first - first) <= EPSILON &&
                    Math.abs(pair.second - second) <= EPSILON;
        }

        @Override
        public int hashCode() {
            return Objects.hash(first, second);
        }

        public Pair(int f, int s) {
            this.first = f;
            this.second = s;
        }
    }
    /**
     *
     * @param a is float number
     * @param b is float number
     *  @return object Pair where first element is bigger than second
     */

    public Pair decrease(int a, int b)
    {

        if (a>=b)
        {
            return new Pair(a,b);

        }
           return new Pair(b,a);

    }


    /**
     *
     * @param Day is day
     * @param Month is month
     * @return object Pair where first element is Day and second is Month
     */

    public Pair nextDate(int Day, int Month)
    {
        assert Day > 0 && Day <= 31 && Month > 0 && Month <= 12 : "Arguments should be more than zero";


        switch (Month) {
            case 1:
                if (Day == 31) {
                    Month++;
                    Day = 1;
                }
                if (Day < 31)
                    Day++;
                break;
            case 3:
                if (Day == 31) {
                    Month++;
                    Day = 1;
                }
                if (Day < 31)
                    Day++;
                break;
            case 5:
                if (Day == 31) {
                    Month++;
                    Day = 1;
                }
                if (Day < 31)
                    Day++;
                break;
            case 7:
                if (Day == 31) {
                    Month++;
                    Day = 1;
                }
                if (Day < 31)
                    Day++;
                break;
            case 8:
                if (Day == 31) {
                    Month++;
                    Day = 1;
                }
                if (Day < 31)
                    Day++;
                break;
            case 10:
                if (Day == 31) {
                    Month++;
                    Day = 1;
                }
                if (Day < 31)
                    Day++;
                break;
            case 12:
                if (Day < 31)
                    Day++;
                else if (Day == 31) {
                    Day = 1;
                    Month = 1;
                }
                break;
            case 2:
                if (Day < 28)
                    Day++;
                if (Day == 28) {
                    Month++;
                    Day = 1;
                }
                break;
            case 4:
                if (Day == 30) {
                    Month++;
                    Day = 1;
                }
                if (Day < 30)
                    Day++;
                break;
            case 6:
                if (Day == 30) {
                    Month++;
                    Day = 1;
                }
                if (Day < 30)
                    Day++;
                break;
            case 9:
                if (Day == 30) {
                    Month++;
                    Day = 1;

                }
                if (Day < 30)
                    Day++;
                break;
            case 11:

                if (Day == 30) {
                    Month++;
                    Day = 1;
                }
                if (Day < 30)
                    Day++;
                break;

            default:
                break;
        }

            return new Pair(Day,Month);

    }

    /**
     *
     * @param a is array of geometric progression's elements
     * @return denominator of geometric progression
     */
    //додати перевірку z через точність Е
    public float checkProgression(int a[])
    {
        int n= a.length;

        for (int j=0; j<n; ++j) {
            assert a[j] != 0 : "Argument should not be zero";
        }


        float z=a[1]/a[0];
        for (int i=1; i<n; ++i) {
            if (Math.abs(z-(a[i]/a[i-1]))>EPSILON) {
                return 0;
            }
        }

        return z;
    }

    /**
     *
     * @param Matrix is 2-D matrix
     * @return Matrix with swapped last column and first column where all elements are <0
     */
//
    public int[][] swap(int[][] Matrix)
    {
        int N=Matrix.length;
        int M=Matrix[0].length;
        assert M > 0 && N>0 : "Arguments should be more than zero" ;
        int num=-1,i,j;

        for (j=0; j<M-1; ++j){
            for ( i=0; i<N; ++i)
                if (Matrix[i][j]>=0) break;
            if (i==N){
                num=j;
                break;
            }
        }

        if (num>=0)
            for (i=0;i<N;++i){
                if (Matrix[i][M-1]!=Matrix[i][num]){
                    Matrix[i][M-1]+=Matrix[i][num];
                    Matrix[i][num]=Matrix[i][M-1]-Matrix[i][num];
                    Matrix[i][M-1]-=Matrix[i][num];
                }

            }

        return Matrix;
    }

    /**
     *
     * @param A is integer number
     * @param B is integer number
     * @return true - if numbers are odd, and false - if numbers are even
     */
    public boolean oddness(int A, int B)
    {

            return A % 2 != 0 && B % 2 != 0;
    }

    /**
     *
     * @param A is integer number
     * @param B is integer number
     * @return product of numbers from A to B
     */
    public int product(int A, int B)
    {
        assert A<B :"B should be bigger than A";
        int product=1;
        for (int i = A; i <= B; ++i)
        {
            product *= i;
        }
        return product;
    }

    /**
     *
     * @param N is integer number
     * @return K if K²<=N
     */
    public int searchK(int N)
    {
        assert N>0 : "Argument should be more than zero";
        int K = 1;
        while (K*K <= N) {
            K++;
        }
        K--;
        return K;
    }

    /**
     *
     * @param twoDigitNumber is two-digit integer number
     * @return
     */
    //гратись лиш з сhar
    public int inversion (int twoDigitNumber)
    {
        assert twoDigitNumber>=10  : "Argument should be zero or more";
        String buf = Integer.toString(twoDigitNumber);
        char[] stringToArray = new char[2];
        stringToArray = buf.toCharArray();
        if (stringToArray[1]=='0') buf=String.valueOf(stringToArray[0]);
        else
        buf = String.valueOf(stringToArray[1]) + String.valueOf(stringToArray[0]);
        twoDigitNumber = Integer.valueOf(buf);
        return twoDigitNumber;
    }


}
